#ifndef _SENSOR_DS18B20
#define _SENSOR_DS18B20

#include <device.h>

#define SYS_LOG_DOMAIN "DS18B20"
#define SYS_LOG_LEVEL CONFIG_SYS_LOG_SENSOR_LEVEL
#include <logging/sys_log.h>

struct ds18b20_data {
	struct device *gpio;
	u8_t sample[4];
};

#endif
