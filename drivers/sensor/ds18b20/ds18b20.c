#include <device.h>
#include <gpio.h>
#include <misc/byteorder.h>
#include <misc/util.h>
#include <sensor.h>
#include <string.h>
#include <zephyr.h>

#include <board.h>
#include <gpio.h>
#include "stm32f4xx_hal_ds18b20.h"
#include "stm32f4xx_hal_onewire.h"
#include "ds18b20.h"

#define EXPECTING_SENSORS    1
TM_OneWire_t OW;
uint8_t device[EXPECTING_SENSORS][8];
uint8_t alarm_device[EXPECTING_SENSORS][8];
float temps[EXPECTING_SENSORS];

static int ds18b20_sample_fetch(struct device *dev, enum sensor_channel chan)
{
	/* Check if connected device is DS18B20 */
	if (TM_DS18B20_Is(DS_ROM)) {
		/* Everything is done */
		if (TM_DS18B20_AllDone(&OW)) {
			/* Read temperature from device */
			if (TM_DS18B20_Read(&OW, DS_ROM, &temp)) {
				/* Start again on all sensors */
				TM_DS18B20_StartAll(&OW);
			} else {
				SYS_LOG_ERR("Fatal error on line");
			}
		}
	}
}

static int ds18b20_channel_get(struct device *dev,
			   enum sensor_channel chan,
			   struct sensor_value *val)
{
	struct dht_data *drv_data = dev->driver_data;

	__ASSERT_NO_MSG(chan == SENSOR_CHAN_TEMP);

	if (chan == SENSOR_CHAN_TEMP) {
		/*TODO PROCESS DATA HERE */
	}

	return 0;

}

static const struct sensor_driver_api ds18b20_api = {
	.sample_fetch = &ds18b20_sample_fetch,
	.channel_get = &ds18b20_channel_get,
};

static int ds18b20_init(struct device *dev)
{
	struct device *data_pin;

	data_pin = device_get_binding(DS18B20_PORT);
	if (data_pin == NULL) {
		SYS_LOG_ERR("Failed to get GPIO device.");
		return -EINVAL;
	}

	gpio_pin_configure(data_pin, DS18B20_PIN, GPIO_DIR_IN);
	TM_OneWire_Init(&OW, DS18B20_PORT, DS18B20_PIN);	

	if (TM_OneWire_First(&OW)) {
		/* Read ROM number */
		TM_OneWire_GetFullROM(&OW, DS_ROM);
		SYS_LOG_DBG("ROM read");
	} else {
		SYS_LOG_ERR("Failed to establish OW connection");
	}

	if (TM_DS18B20_Is(DS_ROM)) {
		/* Set resolution */
		TM_DS18B20_SetResolution(&OW, DS_ROM,
		TM_DS18B20_Resolution_12bits);
		
		/* Set high and low alarms */
		TM_DS18B20_SetAlarmHighTemperature(&OW, DS_ROM, 30);
		TM_DS18B20_SetAlarmLowTemperature(&OW, DS_ROM, 10);
		
		/* Start conversion on all sensors */
		TM_DS18B20_StartAll(&OW);
	}

	return 0;
}

struct ds18b20_data ds18b20_data;

DEVICE_INIT(ds18b20_dev, CONFIG_DS18B20_NAME, &ds18b20_init, &ds18b20_data,
	    NULL, POST_KERNEL, CONFIG_SENSOR_INIT_PRIORITY);
